import React from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Image, Text, TouchableOpacity} from 'react-native';

import styles from './Styles';
import b1 from '../../assets/b1.jpg';
import b2 from '../../assets/b2.jpg';
import b3 from '../../assets/b3.jpg';
import logo from '../../assets/logo.png';


export default function ImageGallery() {
  return (
    <>
    <View style={styles.container}>
      <Image source={logo} style={{width:150, height:70, marginTop:110}} />
      </View>
      <View>
      <ScrollView horizontal ={true} contentContainerStyle={{flexGrow : 1, justifyContent : 'center', alignItems:'center'}} 
            decelerationRate={0}
            snapToInterval={330} 
            snapToAlignment={"center"}
            showsHorizontalScrollIndicator={false}
        >
        <View style={styles.image1} >
        <Image source={b1} resizeMode="contain" style={styles.image} />
        <Text style= {styles.text}>...</Text>
        <Text style= {styles.text}>Crie Aplicativos incriveis</Text>
        <Text style= {styles.text}>
         para sua empresa
         </Text>
        </View>
        <View style={styles.imgView} >
        <Image source={b2} resizeMode="contain" style={styles.image} />
        <Text style= {styles.text}>...</Text>
        <Text style= {styles.text}>Agrege valor a sua StartUp</Text>
        </View>
        <View style={styles.lastImage} >
        <Image source={b3} resizeMode="contain" style={styles.image} />
        <Text style= {styles.text}>...</Text>
        <Text style= {styles.text}>Conheça algumas empresas
        </Text>
        <Text style= {styles.text}>
         que trabalham conosco
        </Text>
        </View>
      </ScrollView>
      </View>
      </>
  );
}
