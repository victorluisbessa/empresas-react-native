import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import React from 'react';
import {MaterialIcons} from 'react-native-vector-icons';
import Login from '../src/Pages/login/Login';
import noConnection from '../src/Pages/noConnection/noConnection';
import Principal from '../src/Pages/principal/Principal';
import Empresas from '../src/Pages/empresas/Empresas';


const Bottom = createBottomTabNavigator(
  {
   Empresas: {
      screen:Empresas,
      navigationOptions: {
        tabBarLabel: 'Empresas',
        activeTintColor: '#444',
        inactiveTintColor: '#444',
        tabBarIcon: ({ }) => <MaterialIcons name='business' size={26} style={{ color: '#444' }} />
      },
    },

  } );



const Routes = createAppContainer(
  createSwitchNavigator({
    Principal,
    Login,
    noConnection,
    Bottom,
  })
);
export default Routes;