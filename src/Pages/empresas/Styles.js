import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  iView: {
    alignItems:'center',
    justifyContent:'center',
    marginHorizontal: 45,
    width: 240,
    height: 362,
    borderRadius: 20,
    marginTop: 10,
    backgroundColor:"#ddd",
    marginLeft: 45,
  },
  gView:{
    alignItems:'center',
    justifyContent:'center',
    flex:1,
  },
  iText: {
   color: '#444',
   fontWeight: 'bold',
   fontSize: 16
  },
  imgView: {
   justifyContent: 'center',
   alignItems: 'center',
   marginTop: 50,
   marginBottom: 10,
  },
  iconView: {
    marginTop: 10,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  sloganText:{
    fontWeight:'bold',
    fontSize: 18,
  },
  button: {
    height: 42,
    backgroundColor: '#FFFF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
    marginBottom: 10,
  },
  });