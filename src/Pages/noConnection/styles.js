import { StyleSheet } from 'react-native';

// import { Container } from './styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});