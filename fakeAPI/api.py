####Criar API falsa para desenvolvimento devido ao insucesso ao conectar na API fornecida ############
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import random as rnd
import string
import time
import sys
from datetime import date
app = Flask(__name__)


@app.route('/api/v1/users/auth/sign_in', methods=['POST'])
def add_message():
    content = request.json
    if content['email'] == "testeapple@ioasys.com.br" and content['password'] == "12341234":
        time.sleep(2)
        return jsonify({"acessToken":"tokendeacesso","uid":"001","client":"001", "error":"false"})
    else:
        time.sleep(2)
        return jsonify({'passerror':'true'})

@app.route('/api/v1/enterprises', methods=['GET','POST'])
def enterprises():
    content = request.json
    empresas = ["caixa", "bancoInter", "localiza", "viajanet", "cisco", "valourec"]
    imagens = ["https://www.ioasys.com.br/images/caixa.png", "https://www.ioasys.com.br/images/bancointer.png", "https://www.ioasys.com.br/images/localiza.png", "https://www.ioasys.com.br/images/viajanet.png", "https://www.ioasys.com.br/images/cisco.png", "https://www.ioasys.com.br/images/vallourec.png"]
    links = ["https://www.caixa.com.br","https://www.bancointer.com.br","https://www.localiza.com.br","https://www.viajanet.com.br","https://www.cisco.com.br","https://www.valourec.com.br"]
    data = []
    if content['acessToken'] == "tokendeacesso" and content['uid'] == "001" and content['client'] == "001":
        empresa_id = request.args.get('empresas')
        app.logger.info(empresa_id)
        if empresa_id is not None:
            if empresa_id in empresas:
                data.append({
                'erro': 'false',
                'empresa': empresa_id,
                'comentario': 'essa é uma empresa que confia nos serviços Ioasys',
                'autor':'Autor não conhecido por ser ApiFake :(',
                'data': str(date.today())
                })
            else:
                 data.append({
                'erro':'true'
                })
        else:      
            for x in range(5):
                data.append({
                'empresa':empresas[x],
                'imagem':imagens[x],
                'link':links[x] ,
                'id': str(x)
                })
        time.sleep(2)
        return jsonify(data)
        

if __name__ == '__main__':
    app.run(host= '0.0.0.0',debug=True)
