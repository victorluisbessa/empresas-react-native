import axios from 'axios';

const api = axios.create({
  //IP da Máquina rodando a Fake API, Mudar para seu IP pois Android não reconhece LocalHost
  baseURL: 'http://172.16.1.100:5000'
});
export default api;