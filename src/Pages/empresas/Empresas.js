//importação módulos
import React, {useState, useEffect} from 'react';
import {View, AsyncStorage, Image, Linking, Text, TouchableOpacity, FlatList} from 'react-native';
import LottieView from 'lottie-react-native';


//importação arquivos
import Logo from '../../assets/logo.png';
import api from '../../Services/api';
import styles from './Styles.js';
import load from '../../assets/load.json';

function Item({ item }) {
  return (
    <TouchableOpacity onPress={() => Linking.openURL(item.link)}>
    <>
    <View style={styles.gView}>
    <View style={styles.iView}>
    <Image source={{uri: item.imagem}}
       style={{width: 150, height: 54}} 
       resizeMode='contain' />
      <Text style= {styles.iText}>{item.empresa}</Text>
      </View>
    </View>
    </>
    </TouchableOpacity>
  );
}


export default function Empresas({navigation}) {
  const [acessToken, setToken] = useState();
  const [uid, setUid] = useState();
  const [client, setClient] = useState();
  const [empresas, setEmpresas] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    async function get_serial(){
      setLoading(true);
      AsyncStorage.getItem('acessToken').then(token => {
      setToken(token);
     });
     AsyncStorage.getItem('client').then(cli => {
      setClient(cli);
   });
   AsyncStorage.getItem('uid').then(uidd => {
      setUid(uidd);
 });
  }

  async function loadEnterprise(){
    const response = await api.post('/api/v1/enterprises', {
      "acessToken": "tokendeacesso",
      "client":"001",
      "uid":"001"
    });
    setEmpresas(response.data);
    setLoading(false);
   }
  
  console.log(loading);
  get_serial();
  loadEnterprise();
   }, []);

   function searchNavigate(){
    navigation.navigate('BuscarEmpresas');
  }

  return (
    <View style={styles.container}>
       <Image source={Logo} style={{width:150, height:70, marginTop:110}}/>
       <Text style={styles.sloganText}>Algumas Empresas</Text>
       <Text style={styles.sloganText}>Que Trabalham Conosco</Text>
       {loading ?<LottieView source={load} style={{position:'absolute'}} autoPlay loop />:<></>}
       <FlatList
        decelerationRate={0}
        snapToInterval={330}
        snapToAlignment={"center"}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={empresas}
        renderItem={({ item }) => <Item item={item} />}
        keyExtractor={item => String(item.id)}
      />
    </View>
  );
   }
