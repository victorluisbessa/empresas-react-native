import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgView: {
    alignItems:'center',
    justifyContent:'center',
    marginHorizontal: 45,
    marginTop: 20,
  },
  image:{
    width: 240,
    height: 362,
    borderRadius: 20
  },
  image1:{
    alignItems:'center',
    justifyContent:'center',
    marginHorizontal: 45,
    marginLeft: 90,
    marginTop: 20,
  },
  lastImage:{
    alignItems:'center',
    justifyContent:'center',
    marginLeft: 45,
    marginRight: 90,
    marginTop: 20,
  },
  text:{
    fontWeight:'bold',
    fontSize: 16,
    color:'#494F5F',
    marginTop: 10,
  },
  
  });