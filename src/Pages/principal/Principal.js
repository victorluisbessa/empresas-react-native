//importação módulos
import React, {useState, useEffect} from 'react';
import {View,KeyboardAvoidingView, AsyncStorage, Image, Text, StyleSheet, TextInput, TouchableOpacity,  Alert, NetInfo,  Platform, ToastAndroid, ScrollView} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

//importação arquivos
import Logo from '../../assets/logo.png';
import api from '../../Services/api';
import styles from './Styles.js';
import ImageGallery from '../../components/imageGallery/ImageGallery';


export default function principal({navigation}) {
  function navig(){
    navigation.navigate('Login');
  }
  return (
    <>
    <ImageGallery/>
    <TouchableOpacity onPress= {navig} style={styles.button}>
          <Text style={styles.buttonText}>Começar</Text>
    </TouchableOpacity>
    </>
  );
}
