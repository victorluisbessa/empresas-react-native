//importação módulos
import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';

//importação assets
import Logo from '../../assets/logo.png';

//exportação função
export default function noConnection(){
  return <View style={styles.container}> 
    <Image source={Logo} />
    <MaterialIcons name='vpn-lock' size={40} color="#000000" />
  </View>
 }


