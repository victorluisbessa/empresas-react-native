//importação módulos
import React, {useState, useEffect} from 'react';
import {View,KeyboardAvoidingView, AsyncStorage, Image, Text, StyleSheet, TextInput, TouchableOpacity,  Alert, NetInfo,  Platform, ToastAndroid} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import LottieView from 'lottie-react-native';

//importação arquivos
import Logo from '../../assets/logo.png';
import api from '../../Services/api';
import styles from './Styles.js';
import fingerprint from '../../assets/digital.json';
import wrong from '../../assets/wrong.json';

//função principal (tela)
export default function Login({navigation}){
//checar a conexão com a internet
  CheckConnectivity = () => {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected === false) {
          //se a plataforma for Android mostrar Toast
          if (Platform.OS === "android") {
            ToastAndroid.show('Sem conexão com a internet', ToastAndroid.SHORT);
            navigation.navigate('noConnection');
           }
           //Se for Ios mostrar alerta
           else{
          Alert.alert("Sem conexão com a rede");
          navigation.navigate('noConnection');
        }
      }
      });
    }   
  
 //chamar funções 
  CheckConnectivity();
  //states
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [passwordError, setError]= useState(false);

  //Hook do UseEffect Semelhante ao ComponentDidMount()
  useEffect(() => {
      //Usuário ja logado?
      AsyncStorage.getItem('acessToken').then(acessToken => {
        if (acessToken) {
          if (Platform.OS === "android") {
            ToastAndroid.show('Usuário já logado', ToastAndroid.SHORT);
           }
          //navigation.navigate('Empresas');
        }
      })
  }, []);
  //handle do submit do login
  async function handleSubmit() {
    //state para mostrar indicação
      setLoading(true);
      //tentar logar
      try {
      //Post com axios na apiLogin
      console.log("conectando");
     const response = await api.post('/api/v1/users/auth/sign_in', {
       'email': email,
       'password': password
     });
     //recebe a resposta da Api
     const {acessToken, client, uid, passerror} = response.data;
     if (passerror){
       setError(true);
       timeoutCheck = setTimeout(() => {
       setError(false);
        }, 2000);
     }
     console.log(acessToken);
     //Fim do carregamento
     setLoading(false);
          //salva os dados na memória local do aparelho usando AsyncStorage
          await AsyncStorage.setItem('acessToken', acessToken);
          await AsyncStorage.setItem('client', client);
          await AsyncStorage.setItem('uid', uid);
          navigation.navigate('Empresas');

      }
  //Erro na requisição
  catch (error) {
    //Console do Erro DEBUG
    console.log(error);
    //android
    if (Platform.OS === "android") {
      ToastAndroid.show('Erro na conexão com servidor', ToastAndroid.SHORT);
     }
     //ios
     else{
        Alert.alert("Erro na conexão com servidor,   por favor tente mais tarde :(");
     }
  }
}

///JSX JSX JSX JSX JSX JSX JSX JSX JSX JSX JSX 
  return <KeyboardAvoidingView behavior="padding" style={styles.container}>
    <Image source={Logo}/>
    <KeyboardAvoidingView style={styles.form}>
        <Text style={styles.email}>Seu Email</Text>
        <TextInput 
            style={styles.input}
            placeholder="Email cadastrado"
            placeholderTextColor="#999"
            keyboardType="email-address"
            autoCapitalize='none'
            autoCorrect={false}
            value={email}
            onChangeText= {setEmail}
        />
        <Text style={styles.email}>Senha</Text>
         <TextInput 
            style={styles.input}
            placeholder="Senha"
            placeholderTextColor="#999"
            keyboardType="numeric"
            autoCapitalize='none'
            autoCorrect={false}
            value={password}
            onChangeText= {setPassword}
        />
        <TouchableOpacity onPress= {handleSubmit} style={styles.button}>
          <Text style={styles.buttonText}>Entrar</Text>
        </TouchableOpacity>
    </KeyboardAvoidingView>
    <View style={styles.icone}>
      <SimpleLineIcons name='lock' size={30} color="#000000" />
    </View>
    {loading ?<LottieView source={fingerprint} style={{position:'absolute'}} autoPlay loop />:<></>}
    {passwordError ?<LottieView source={wrong} style={{position:'absolute'}} autoPlay/>:<></>}
  </KeyboardAvoidingView>
}

