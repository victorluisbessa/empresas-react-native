import { StyleSheet } from 'react-native';

export default StyleSheet.create({
button: {
  height: 42,
  backgroundColor: '#494F5F',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 6,
  marginTop: 50,
  alignSelf: 'center',
  width: 240,
  height: 60,
},
buttonText: {
  color: '#FFF',
  fontWeight: 'bold',
  fontSize: 16
},
container:{
  flex:1,
  alignItems:'center',
  justifyContent:'center',
},
});